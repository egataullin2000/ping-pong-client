package application;

import game.Game;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;
import menu.Menu;

public class App extends Application {

    public static final int WINDOW_WIDTH = 400;
    public static final int WINDOW_HEIGHT = 400;

    private Stage primaryStage;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        primaryStage.setTitle("Ping-pong");
        goToMenu();
        primaryStage.show();
    }

    public void setScene(Scene scene) {
        Platform.runLater(() -> primaryStage.setScene(scene));
    }

    public void goToMenu() {
        Scene menu = new Menu(this);
        primaryStage.setScene(menu);
    }

    public void goToGame() {
        new Game(this);
    }

}
