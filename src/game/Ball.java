package game;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import java.awt.*;

public class Ball extends Circle {

    private static final int RADIUS = 10;

    public Ball(int x, int y) {
        super(x, y, RADIUS);
        setFill(Color.BLUE);
    }

    public void move(int destX, int destY) {
        setCenterX(destX);
        setCenterY(destY);
    }

}
