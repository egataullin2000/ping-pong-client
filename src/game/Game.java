package game;

import application.App;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import network.Commands;
import network.ServerConnection;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;

import java.io.IOException;

public class Game implements GameInterface {

    private App app;
    private Scene gameScene;
    private int playerNumber;
    private boolean isFinished = false;
    private Score score = new Score();
    private GameField gameField;
    private ServerConnection connection;
    private CommandDispatcher commandDispatcher = new CommandDispatcher(this);
    private Thread receiverThread = new Thread(() -> {
        while (!isFinished) commandDispatcher.dispatch(connection.receive());
    });

    public Game(App app) {
        this.app = app;
        try {
            connection = new ServerConnection();
            receiverThread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start() {
        onStart();
    }

    @Override
    public void onStart() {
        gameField = new GameField(this, App.WINDOW_WIDTH, App.WINDOW_HEIGHT);
        gameScene = new Scene(gameField, App.WINDOW_WIDTH, App.WINDOW_HEIGHT);
        gameScene.setOnKeyPressed(keyEvent -> {
            KeyCode keyCode = keyEvent.getCode();

            Direction direction = null;
            if (keyCode == KeyCode.W || keyCode == KeyCode.UP) {
                direction = Direction.UP;
            } else if (keyCode == KeyCode.S || keyCode == KeyCode.DOWN) {
                direction = Direction.DOWN;
            }

            String command = Commands.COMMAND_MOVE_PLAYER + " " + playerNumber + " " + direction;
            connection.send(command);
        });
        app.setScene(gameScene);
    }

    @Override
    public void onWin() {
        Node msg = generateMsgView("YOU WON!");
        msg.setStyle("-fx-fill: green;-fx-font-size: 64");
        gameField.getChildren().add(msg);
    }

    @Override
    public void onLose() {
        Node msg = generateMsgView("YOU LOSE");
        msg.setStyle("-fx-fill: red;-fx-font-size: 64");
        gameField.getChildren().add(msg);
    }

    public Node generateMsgView(String message) {
        Text text = new Text(0, App.WINDOW_HEIGHT / 2.0, message);
        text.setWrappingWidth(App.WINDOW_WIDTH);
        text.setTextAlignment(TextAlignment.CENTER);

        return text;
    }

    @Override
    public void onFinish(int winnerNumber) {
        isFinished = true;
        receiverThread.interrupt();
        connection.close();
        Platform.runLater(() -> {
            if (winnerNumber == playerNumber) {
                onWin();
            } else onLose();
        });
        gameScene.addEventHandler(MouseEvent.MOUSE_CLICKED, (mouseEvent) -> app.goToMenu());
    }

    @Override
    public int getPlayerNumber() {
        return playerNumber;
    }

    public void setPlayerNumber(int number) {
        playerNumber = number;
    }

    @Override
    public Score getScore() {
        return score;
    }

    public void movePlayer(int playerNumber, int destX, int destY) {
        gameField.movePlayer(playerNumber, destX, destY);
    }

    public void moveBall(int destX, int destY) {
        gameField.moveBall(destX, destY);
    }

    public void scoreUp(int playerNumber) {
        score.up(playerNumber);
    }

}
