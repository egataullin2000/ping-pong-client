package game;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Score {

    private int[] balls = new int[2];
    private StringProperty text = new SimpleStringProperty("0:0");

    public void up(int playerNumber) {
        balls[playerNumber]++;
        text.setValue(balls[0] + ":" + balls[1]);
    }

    public StringProperty getText() {
        return text;
    }

    @Override
    public String toString() {
        return text.get();
    }
}
