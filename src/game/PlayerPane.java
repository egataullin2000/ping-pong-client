package game;

import javafx.scene.shape.Rectangle;

import java.awt.*;

public class PlayerPane extends Rectangle {

    public static final int WIDTH = 20;
    public static final int HEIGHT = 150;

    public PlayerPane(Point position) {
        super(position.x, position.y, WIDTH, HEIGHT);
    }

    public void move(int destX, int destY) {
        setX(destX);
        setY(destY);
    }

}
