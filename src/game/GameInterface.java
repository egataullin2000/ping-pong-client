package game;

public interface GameInterface {
    void onStart();

    void onWin();

    void onLose();

    void onFinish(int winnerNumber);

    int getPlayerNumber();

    Score getScore();
}
