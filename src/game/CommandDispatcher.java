package game;

import java.util.Scanner;

import static network.Commands.*;

public class CommandDispatcher {

    private Game game;

    public CommandDispatcher(Game game) {
        this.game = game;
    }

    public void dispatch(String command) {
        if (command == null) return;

        Scanner scanner = new Scanner(command);
        command = scanner.next();
        if (COMMAND_MOVE_BALL.equals(command)) {
            game.moveBall(
                    scanner.nextInt(), scanner.nextInt()
            );
        } else if (COMMAND_MOVE_PLAYER.equals(command)) {
            game.movePlayer(
                    scanner.nextInt(),
                    scanner.nextInt(),
                    scanner.nextInt()
            );
        } else if (COMMAND_SCORE_UP.equals(command)) {
            game.scoreUp(scanner.nextInt());
        } else if (COMMAND_START.equals(command)) {
            game.start();
        } else if (COMMAND_SET_PLAYER_NUMBER.equals(command)) {
            game.setPlayerNumber(scanner.nextInt());
        } else if (COMMAND_FINISH.equals(command)) {
            game.onFinish(scanner.nextInt());
        }

        scanner.close();
    }

}
