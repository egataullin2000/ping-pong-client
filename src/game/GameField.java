package game;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import java.awt.*;

public class GameField extends Pane {

    private PlayerPane[] players = new PlayerPane[2];
    private Ball ball;

    public GameField(GameInterface game, int width, int height) {
        super();

        int x, y;

        x = 0;
        y = height / 2 - PlayerPane.HEIGHT / 2;
        players[0] = new PlayerPane(new Point(x, y));
        players[0].setFill(Color.RED);

        x = width - PlayerPane.WIDTH;
        players[1] = new PlayerPane(new Point(x, y));
        players[1].setFill(Color.RED);

        players[game.getPlayerNumber()].setFill(Color.GREEN);

        x = width / 2;
        y = height / 2;
        ball = new Ball(x, y);
        ball.setFill(Color.BLUE);

        Score score = game.getScore();
        Text scoreView = new Text(0, 24, score.toString());
        scoreView.setWrappingWidth(width);
        scoreView.setTextAlignment(TextAlignment.CENTER);
        scoreView.textProperty().bind(score.getText());
        scoreView.setStyle("-fx-font-size: 24");

        getChildren().addAll(players);
        getChildren().addAll(ball, scoreView);
    }

    public void movePlayer(int playerNumber, int destX, int destY) {
        players[playerNumber].move(destX, destY);
    }

    public void moveBall(int destX, int destY) {
        ball.move(destX, destY);
    }

}
