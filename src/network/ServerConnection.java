package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ServerConnection {

    private static final String SERVER_IP = "localhost";
    private static final int SERVER_PORT = 4444;

    private Socket socket;
    private BufferedReader input;
    private PrintWriter output;

    public ServerConnection() throws IOException {
        socket = new Socket(SERVER_IP, SERVER_PORT);
        input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        output = new PrintWriter(socket.getOutputStream(), true);
    }

    public void send(String command) {
        output.println(command);
    }

    public String receive() {
        String command = null;
        try {
            command = input.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return command;
    }

    public void close() {
        try {
            input.close();
            output.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
