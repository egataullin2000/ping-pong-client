package menu;

import application.App;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

public class Menu extends Scene {

    public Menu(App app) {
        super(new GridPane(), App.WINDOW_WIDTH, App.WINDOW_HEIGHT);
        GridPane pane = (GridPane) getRoot();
        pane.setAlignment(Pos.CENTER);

        Button button = new Button("Start");
        button.setStyle("-fx-font-size: 24");
        button.addEventHandler(MouseEvent.MOUSE_CLICKED, (mouseEvent) -> {
            app.goToGame();
            button.setText("Waiting for server");
            button.setDisable(true);
        });

        pane.getChildren().add(button);
    }

}
